'use strict';
module.exports = (sequelize, DataTypes) => {
  const buku = sequelize.define('buku', {
    title: DataTypes.STRING,
    author: DataTypes.STRING,
    published_date: DataTypes.DATE,
    pages: DataTypes.INTEGER,
    langguage: DataTypes.STRING,
    publisher_id: DataTypes.STRING
  }, {});
  buku.associate = function(models) {
    // associations can be defined here
  };
  return buku;
};