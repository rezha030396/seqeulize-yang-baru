var model = require("../models/index");

module.exports = function(app) {
  /* GET todo listing. */
  app.get("/bukus", function(req, res, next) {
    model.buku.findAll({})
      .then(bukus=>
        res.json({
          error: false,
          data: bukus
        })
      )
      .catch(error =>
        res.json({
          error: true,
          data: [],
          error: error
        })
      );
  });



  app.get("/bukus/:id", function(req, res, next) {
   const buku_id = req.params.id
    model.buku.findAll({
        where:{
            id:buku_id
        }
    })
      .then(bukus=>
        res.json({
          error: false,
          data: bukus
        })
      )
      .catch(error =>
        res.json({
          error: true,
          data: [],
          error: error
        })
      );
  });



//   /* POST todo. */
  app.post("/bukus", function(req, res, next) {
    const { title, author,published_date,pages,langguage,publisher_id } = req.body;

        if (title == null) {
            res.status(400).json({
                error: true,
                message: "TITLE BELUM DI ISI !!!"
              })
        }

    model.buku.create({
      title: title,
      author: author,
      published_date:published_date,
      pages:pages,
      langguage:langguage,
      publisher_id:publisher_id
    })
      .then(buku =>
        res.status(201).json({
          error: false,
          data: buku,
          message: "New todo has been created."
        })
      )
      .catch(error =>
        res.json({
          error: true,
          data: [],
          error: error
        })
      );
  });

//   /* update todo. */
  app.put("/bukus/:id", function(req, res, next) {
    const buku_id = req.params.id;

    const { title, author,published_date,pages,langguage,publisher_id } = req.body;

    model.buku.update(
      {
        title: title,
        author: author,
      published_date:published_date,
      pages:pages,
      langguage:langguage,
      publisher_id:publisher_id
      },
      {
        where: {
          id: buku_id
        }
      }
    )
      .then(buku => res.json({
          error: false,
          message: "buku has been updated.",
          
        
        })
      )
      .catch(error =>
        res.json({
          error: true,
          error: error
        })
      );
  });

//   /* GET todo listing. */
  /* Delete todo. */
  app.delete("/bukus/:id", function(req, res, next) {
    const buku_id = req.params.id;

    model.buku.destroy({
      where: {
        id: buku_id
      }
    })
      .then(status =>
        res.json({
          error: false,
          message: "buku has been delete."
        })
      )
      .catch(error =>
        res.json({
          error: true,
          error: error
        })
      );
  });
};